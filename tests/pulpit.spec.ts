import { test, expect } from '@playwright/test';
import { loginData } from '../test-data/login.data';
import { LoginPage } from '../pages/login.page';
import { PulpitPage } from '../pages/pulpit.page';

test.describe('Pulpit tests', () => {
  let topUpAmount: string;
  let expectedMessage: string;
  let pulpitPage: PulpitPage;

  test.beforeEach(async ({ page }) => {
    const userId = loginData.userId;
    const userPassword = loginData.password;

    await page.goto('/');

    const loginPage = new LoginPage(page);
    await loginPage.login(userId, userPassword);

    pulpitPage = new PulpitPage(page);
  });

  test('Quick payment with correct data', async ({ page }) => {
    const receiverId = '2';
    const transferTitle = 'pizza';
    const expectedTransferReceiver = 'Chuck Demobankowy';
    topUpAmount = '150';
    expectedMessage = `Przelew wykonany! ${expectedTransferReceiver} - ${topUpAmount},00PLN - ${transferTitle}`;

    await pulpitPage.executeQuickPayment(
      receiverId,
      topUpAmount,
      transferTitle,
    );

    await expect(pulpitPage.expectedMessage).toHaveText(expectedMessage);
  });

  test('Phone recharge with correct data', async ({ page }) => {
    const topUpReceiver = '500 xxx xxx';
    topUpAmount = '40';
    expectedMessage = `Doładowanie wykonane! ${topUpAmount},00PLN na numer ${topUpReceiver}`;

    await pulpitPage.executeMobileTopUp(topUpReceiver, topUpAmount);

    await expect(pulpitPage.expectedMessage).toHaveText(expectedMessage);
  });

  test('Correct balance after phone recharge with correct data', async ({
    page,
  }) => {
    const topUpReceiver = '500 xxx xxx';
    topUpAmount = '40';
    const initialBalance = await page.locator('#money_value').innerText();
    const expectedBalance = Number(initialBalance) - Number(topUpAmount);

    await pulpitPage.executeMobileTopUp(topUpReceiver, topUpAmount);

    await expect(pulpitPage.expectedBalance).toHaveText(`${expectedBalance}`);
  });
});

import { test, expect } from '@playwright/test';
import { loginData } from '../test-data/login.data';
import { LoginPage } from '../pages/login.page';

test.describe('User login to Demobank', () => {
  let expectedErrorMessage: string;
  let loginPage: LoginPage;

  test.beforeEach(async ({ page }) => {
    await page.goto('/');
    loginPage = new LoginPage(page);
  });

  test('Successful login with correct credentials', async ({ page }) => {
    const userId = loginData.userId;
    const userPassword = loginData.password;
    const expectedUserName = 'Jan Demobankowy';

    await loginPage.login(userId, userPassword);

    await expect(loginPage.loginError).toHaveText(expectedUserName);
  });

  test('Unsuccessful login with too short username', async ({ page }) => {
    const incorrectUserId = 'tester';
    expectedErrorMessage = 'identyfikator ma min. 8 znaków';

    await loginPage.loginInput.fill(incorrectUserId);
    await loginPage.passwordInput.click();

    await expect(page.getByTestId('error-login-id')).toHaveText(
      expectedErrorMessage,
    );
  });

  test('Unsuccessful login with too short password', async ({ page }) => {
    const userId = loginData.userId;
    const userPassword = '12345';
    expectedErrorMessage = 'hasło ma min. 8 znaków';

    await loginPage.loginInput.fill(userId);
    await loginPage.passwordInput.fill(userPassword);
    await loginPage.passwordInput.blur();

    await expect(loginPage.passwordError).toHaveText(expectedErrorMessage);
  });
});

# Playground

Sample test cases for learning the Playwright tool.

Currently, the code I use is based on the free Playwright course: https://www.youtube.com/@jaktestowac

Website where I perform the tests: https://demo-bank.vercel.app/

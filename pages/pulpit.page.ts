import { Page } from '@playwright/test';
import { SideMenuComponent } from '../components/side-menu.component';

export class PulpitPage {
  paymentButton: any;
  constructor(private page: Page) {}

  sideMenu = new SideMenuComponent(this.page);

  transferReceiver = this.page.locator('#widget_1_transfer_receiver');
  transferAmount = this.page.locator('#widget_1_transfer_amount');
  transferTitle = this.page.locator('#widget_1_transfer_title');

  executeButton = this.page.locator('#execute_btn');
  closeButton = this.page.getByTestId('close-button');

  expectedMessage = this.page.locator('#show_messages');

  topUpReceiver = this.page.locator('#widget_1_topup_receiver');
  topUpAmount = this.page.locator('#widget_1_topup_amount');
  topUpAgreement = this.page.locator('#widget_1_topup_agreement');
  executePhoneButton = this.page.locator('#execute_phone_btn');

  expectedBalance = this.page.locator('#money_value');

  async executeQuickPayment(
    receiverId: string,
    topUpAmount: string,
    transferTitle: string,
  ): Promise<void> {
    await this.transferReceiver.selectOption(receiverId);
    await this.transferAmount.fill(topUpAmount);
    await this.transferTitle.fill(transferTitle);

    await this.executeButton.click();
    await this.closeButton.click();
  }

  async executeMobileTopUp(
    topUpReceiver: string,
    topUpAmount: string,
  ): Promise<void> {
    await this.topUpReceiver.selectOption(topUpReceiver);
    await this.topUpAmount.fill(topUpAmount);

    await this.topUpAgreement.click();
    await this.executePhoneButton.click();
    await this.closeButton.click();
  }
}
